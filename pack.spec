%global with_debug 1

%if 0%{?with_debug}
%global _find_debuginfo_dwz_opts %{nil}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package   %{nil}
%endif

%if ! 0%{?gobuild:1}
%define gobuild(o:) GO111MODULE=off go build -buildmode pie -compiler gc -tags="rpm_crashtraceback ${BUILDTAGS:-}" -ldflags "${LDFLAGS:-} -B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d ' \\n') -extldflags '-Wl,-z,relro -Wl,-z,now -specs=/usr/lib/rpm/redhat/redhat-hardened-ld '" -a -v %{?**};
%endif

%global provider github
%global provider_tld com
%global project buildpacks
%global repo pack
# https://github.com/buildpacks/pack
%global import_path %{provider}.%{provider_tld}/%{project}/%{repo}
%global git0 https://%{import_path}

%global built_tag v0.22.0
%global built_tag_strip %(b=%{built_tag}; echo ${b:1})

Name: %{repo}
Version: 0.22.0
Release: 2%{?dist}
Summary: Convert code into runnable images
License: ASL 2.0
URL: %{git0}
Source0: %{built_tag}-vendor.tar.gz
ExclusiveArch: %{go_arches}
BuildRequires: golang
BuildRequires: git
BuildRequires: glib2-devel
BuildRequires: glibc-static
BuildRequires: make
Provides: %{name}cli = %{version}-%{release}
Provides: %{name}-cli = %{version}-%{release}

%description
%{name} is a CLI implementation of the Platform Interface Specification
for Cloud Native Buildpacks.

%prep
%autosetup -Sgit -n %{name}-%{built_tag_strip}-vendor

%build
mkdir _build
pushd _build
mkdir -p src/%{provider}.%{provider_tld}/%{project}
ln -s $(dirs +1 -l) src/%{import_path}
popd

mv vendor src

export GO111MODULE=off
export GOPATH=$(pwd)/_build:$(pwd)
export CGO_CFLAGS="-O2 -g -grecord-gcc-switches -pipe -Wall -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -ffat-lto-objects -fexceptions -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -D_GNU_SOURCE -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64"
%ifarch x86_64
export CGO_CFLAGS+=" -m64 -mtune=generic -fcf-protection=full"
%endif
# These extra flags present in %%{optflags} have been skipped for now as they break the build
#export CGO_CFLAGS+=" -flto=auto -Wp,D_GLIBCXX_ASSERTIONS -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1"

%gobuild -o out/%{name} %{import_path}/cmd/%{name}

%install
export GOPATH=$(pwd)/_build:$(pwd):%{gopath}
make DESTDIR=%{buildroot} PREFIX=%{_prefix} install

#define license tag if not already defined
%{!?_licensedir:%global license %doc}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}

%changelog
* Tue Nov 30 2021 Jindrich Novy <jnovy@redhat.com> - 0.22.0-2
- add gating.yaml
- Related: #2000051

* Mon Nov 08 2021 RH Container Bot <rhcontainerbot@fedoraproject.org> - 0.22.0-1
- initial build
- Related: #2000051
